const express = require('express');
const { MongoClient, ObjectId } = require('mongodb');

const app = express();
const port = 3000;
app.use(express.json());

const username = 'admin';
const password = 'admin';
const clusterUrl = 'clustern.3qcmcyk.mongodb.net';
const dbName = 'iot';

const url = `mongodb+srv://${username}:${password}@${clusterUrl}/${dbName}?retryWrites=true&w=majority`; // MongoDB connection URL

const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();
console.log('Connection to MongoDB successful');
const db = client.db("iot");
const collection = db.collection('users');

// Read all documents
app.get('/read', async (req, res) => {
    console.log('Connection to read successful');
    const documents = await collection.find().toArray();
    res.json(documents);
});

// Create a document
app.post('/create', async (req, res) => {
    console.log('Connection to create successful');
    const document = req.body;
    const result = await collection.insertOne(document);
    res.json({ message: 'Document created', id: result.insertedId });
});

// Update a document by ID
app.put('/update/:id', async (req, res) => {
    console.log('Connection to update successful', req.params);
    const id = req.params.id;
    const update = req.body;
    const result = await collection.updateOne({ _id: new ObjectId(id) }, { $set: update });
    res.json({ message: 'Document updated', modifiedCount: result.modifiedCount });
});

// Delete a document by ID
app.delete('/delete/:id', async (req, res) => {
    console.log('Connection to delete successful', req.params);
    const id = req.params.id;
    const result = await collection.deleteOne({ _id: new ObjectId(id) });
    res.json({ message: 'Document deleted', deletedCount: result.deletedCount });
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
