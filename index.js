const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// Connect to MongoDB database
const username = 'admin';
const password = 'admin';
const clusterUrl = 'clustern.3qcmcyk.mongodb.net';
const databaseName = 'iot';

// Construct the connection URL
const connectionUrl = `mongodb+srv://${username}:${password}@${clusterUrl}/${databaseName}?retryWrites=true&w=majority`;

// Connect to MongoDB database
mongoose.connect(connectionUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection;

// Create data schema
const dataSchema = new mongoose.Schema({
    name: String,
    age: Number
});

// Create data model
const Data = mongoose.model('Users', dataSchema);

// Create HTTP server
const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk;
        });
        req.on('end', () => {
            const { name, age } = JSON.parse(body);

            // Create new data object
            const newData = new Data({ name, age });

            // Save data object to the database
            newData.save((err, savedData) => {
                if (err) {
                    console.error('Error saving data: ' + err);
                    res.statusCode = 500;
                    res.end(JSON.stringify({ error: 'Failed to save data' }));
                    return;
                }
                console.log('Data saved successfully');

                // Fetch data from the database
                Data.find({}, (fetchErr, fetchResult) => {
                    if (fetchErr) {
                        console.error('Error fetching data: ' + fetchErr);
                        res.statusCode = 500;
                        res.end(JSON.stringify({ error: 'Failed to fetch data' }));
                        return;
                    }

                    // Send the fetched data as the response
                    res.setHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    res.end(JSON.stringify(fetchResult));
                });
            });
        });
    } else if (req.method === 'GET') {
        // Fetch data from the database
        Data.find({}, (fetchErr, fetchResult) => {
            if (fetchErr) {
                console.error('Error fetching data: ' + fetchErr);
                res.statusCode = 500;
                res.end(JSON.stringify({ error: 'Failed to fetch data' }));
                return;
            }

            // Send the fetched data as the response
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 200;
            res.end(JSON.stringify(fetchResult));
        });
    } else {
        res.statusCode = 404;
        res.end('Not Found');
    }
});

// Start the server
const port = 3000;
server.listen(port, () => {
    console.log('Server started on port ' + port);
});